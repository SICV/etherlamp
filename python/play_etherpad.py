#!/usr/bin/env python
import time
from samplebase import SampleBase
from PIL import Image
from PIL import ImageFont, ImageDraw
import sys
import argparse
import json
from etherpad import changeset_parse, perform_changeset_curline
# from BdfFontFile import BdfFontFile


def listlooper (ll):
    while True:
        for l in ll:
            yield(l)


class ImageScroller(SampleBase):
    def __init__(self, *args, **kwargs):
        super(ImageScroller, self).__init__(*args, **kwargs)
        # self.parser.add_argument("-i", "--image", help="The image to display", default="../../../examples-api-use/runtext.ppm")
        self.parser.add_argument("-t", "--text", help="The text to scroll on the RGB LED panel", default="Hello world!")
        self.parser.add_argument("--pad", type=argparse.FileType("r"), default="../constantcodeofconduct2.etherpad.json")
        self.parser.add_argument("--padname", default="constantcodeofconduct2")
        self.parser.add_argument("--font", default="fonts/6x13.pil")
        self.parser.add_argument("--sleep", type=float, default=0.25)
        self.parser.add_argument("--prechars", type=int, default=10)

    def run(self):
        # if not 'image' in self.__dict__:
        #     self.image = Image.open(self.args.image).convert('RGB')
        # self.image.resize((self.matrix.width, self.matrix.height), Image.ANTIALIAS)
        padname = self.args.padname
        sleeptime = self.args.sleep
        precharslen = self.args.prechars
        data = json.load(self.args.pad)
        pad_data = data["pad:{0}".format(padname)]
        last_rev = pad_data['head']
        cursorcolors = [(255, 255, 0), (255, 0, 0), (255, 0, 255), (0, 255, 0), (0, 255, 255), (0, 0, 255)]
        cursoriter = listlooper(cursorcolors)
        cursorcolor = cursoriter.next()

        # self.image = Image.new("RGB", (self.matrix.width * 2, self.matrix.height), (0,0,0))
        double_buffer = self.matrix.CreateFrameCanvas()
        self.image = Image.new("RGB", (double_buffer.width, double_buffer.height), (0,0,0))
        img_width, img_height = self.image.size
        draw = ImageDraw.ImageDraw(self.image)
        font = ImageFont.load(self.args.font)

        while True:
            text = "\n"
            curline = None
            for r in range(0, last_rev+1):
                d = data["pad:{0}:revs:{1}".format(padname, r)]
                cs = changeset_parse(d['changeset'])
                text, linepos, charpos, inserts = perform_changeset_curline(text, cs)
                
                # change cursor color when linepos changes
                if curline != None and linepos != curline:
                    cursorcolor = cursoriter.next()
                curline = linepos

                lines = text.splitlines()
                if charpos == 0 and linepos > 0:
                    line = lines[linepos-1]
                    charpos = len(line)
                else:
                    line = lines[linepos]

                pre, post = line[:charpos], line[charpos:]
                pre = pre[-precharslen:] # was 10
                post = post[:5]
                # sys.stdout.write("\r{0}|{1}{2}".format(pre, post, " "*64))
                # sys.stdout.flush()
                draw.rectangle((0, 0, img_width, img_height), fill=(0,0,0))
                if inserts:
                    pre, insert = pre[:-inserts], pre[-inserts:]
                    x = 0
                    draw.text((x, 10), pre, font=font)
                    tw, th = font.getsize(pre)
                    x += tw
                    tw, th = font.getsize(insert)
                    draw.rectangle((x, 10, x+tw, 10+th), fill=cursorcolor)
                    draw.text((x, 10), insert, font=font, fill=(0,0,0))
                    x += tw
                    draw.text((x, 10), post, font=font)
                else:
                    # msg = "{0}|{1}".format(pre, post)
                    msg = "{0}{1}".format(pre, post)
                    draw.text((0, 10), msg, font=font)

                double_buffer.SetImage(self.image, 0)
                double_buffer = self.matrix.SwapOnVSync(double_buffer)
                time.sleep(sleeptime)

            draw.rectangle((0, 0, img_width, img_height), fill=(0,0,0))
            double_buffer.SetImage(self.image, 0)
            double_buffer = self.matrix.SwapOnVSync(double_buffer)
            time.sleep(5.0)


# Main function
# e.g. call with
#  sudo ./image-scroller.py --chain=4
# if you have a chain of four
if __name__ == "__main__":
    image_scroller = ImageScroller()
    if (not image_scroller.process()):
        image_scroller.print_help()
