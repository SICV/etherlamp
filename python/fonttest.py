from PIL import Image, ImageDraw, ImageFont


# for s in range(1, 20):

im = Image.new("RGB", (64,32))
# font = ImageFont.truetype("fonts/slkscre.ttf", s)
font = ImageFont.load("fonts/tom-thumb.pil")
draw = ImageDraw.ImageDraw(im)
draw.text((0, 0), "hello world", font=font, fill=(255,255,255))

im.save("fonttest.png")

