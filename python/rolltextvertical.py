#!/usr/bin/env python
# Display a runtext with double-buffering.
from samplebase import SampleBase
from rgbmatrix import graphics
import time

# should be run with --led-pixel-mapper "Rotate:270"

class RunText(SampleBase):
    def __init__(self, *args, **kwargs):
        super(RunText, self).__init__(*args, **kwargs)
        self.parser.add_argument("-t", "--text", help="The text to scroll on the RGB LED panel", default="It could have been otherwise.")
        self.parser.add_argument("-x", "--margin", type=int, help="x position of text", default=0)
        self.parser.add_argument("--uppercase", default=False, action="store_true")
        self.parser.add_argument("--lineheight", type=int, default=18)
        self.parser.add_argument("--font", default="fonts/10x20.bdf")
        self.parser.add_argument("--sleep", type=float, default=0.02)
        self.parser.add_argument("--pause", type=float, default=5.0)

    def run(self):
        offscreen_canvas = self.matrix.CreateFrameCanvas()
        print ("offscreen_canvas, size: {0}x{1}".format(offscreen_canvas.width, offscreen_canvas.height))
        font = graphics.Font()
        # font.LoadFont("fonts/7x13.bdf")
        # font.LoadFont("fonts/9x18.bdf")
        font.LoadFont(self.args.font)
        textColor = graphics.Color(255, 255, 255)
        pos = offscreen_canvas.height
        my_text = self.args.text
        if self.args.uppercase:
            my_text = my_text.upper()
        line_height = self.args.lineheight
        txtlen = (len(my_text)+1)*line_height
        sleeptime = self.args.sleep
        while True:
            offscreen_canvas.Clear()
            # len = graphics.DrawText(offscreen_canvas, font, pos, 20, textColor, my_text)
            y = pos + line_height
            for char in my_text:
                cw = font.CharacterWidth(ord(char))
                cx = self.args.margin + ((offscreen_canvas.width - cw) / 2)
                graphics.DrawText(offscreen_canvas, font, cx, y, textColor, char)
                y += line_height
            pos -= 1
            if (pos + txtlen < 0):
                pos = offscreen_canvas.height
                time.sleep(self.args.pause)

            time.sleep(sleeptime)
            offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)


# Main function
if __name__ == "__main__":
    run_text = RunText()
    if (not run_text.process()):
        run_text.print_help()
