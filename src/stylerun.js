export class StyleRun {

	static render_runs (obj, text) {
		var runs = [];
		var allintervals = [];
		var by_pos = {};
		for (var key in obj) {
			let run = obj[key];
			console.log("run", key);
			console.log(run.unparse());
			console.log("---");
			runs.push(run);
			for (var i=0, len=run.runs.length; i<len; i++) {
				let interval = run.runs[i];
				interval.attr = key;
				
				if (by_pos[interval.start] === undefined) {
					by_pos[interval.start] = {starts: [], ends: [], value: interval.start}
				}
				by_pos[interval.start].starts.push(interval);

				if (by_pos[interval.end] === undefined) {
					by_pos[interval.end] = {starts: [], ends: [], value: interval.end }
				}
				by_pos[interval.end].ends.push(interval);

				allintervals.push(interval);
			}
		}
		var nodes = [];
		for (var key in by_pos) {
			nodes.push(by_pos[key]);
		}
		nodes.sort((a, b) => {
			return a.value - b.value;
		});

		function to_list (set) {
			return Array.from(set);
			// return [...set];
			// let array = [];
			// set.forEach(x => array.push(x));
		}

		console.log("render_runs", text.length, text);
		console.log("nodes", nodes);
		var r = '',
			last_node_pos = undefined,
			attrs = new Set(),
			spans = [];
		for (var i=0, len=nodes.length; i<len; i++) {
			let node = nodes[i];
			if (last_node_pos!== undefined) {
				spans.push({
					text: text.substring(last_node_pos, node.value),
					classlist: to_list(attrs).map(x => `attr${x}`).join(" ")
				});
			}
			last_node_pos = node.value;
			node.ends.forEach(x => attrs.delete(x.attr));
			node.starts.forEach(x => attrs.add(x.attr));
		}
		console.log("rendering", spans);
		return spans.map(x => `<span class="${x.classlist}">${x.text}</span>`).join("");

		// console.log("got", runs.length, "runs", allintervals.length, "total intervals");
		// allintervals.sort((a, b) => {
		// 	return a.start - b.start;
		// });
		// console.log("allintervals", allintervals);		

	}

	constructor () {
		this.runs = [];
	}
	add (start, end) {
		let i=0,
			len = this.runs.length,
			run;

		for (; i<len; i++) {
			run = this.runs[i];
			if (run.start > end) {
				// incoming interval is fully before
				// insert incoming interval before i
				this.runs.splice(i, 0, {start: start, end: end});
				return;
			}
			if (start < run.end) {
				// eventually extend start (left) -- "union"
				run.start = Math.min(start, run.start);
				// new interval intersects i
				if (end <= run.end) {
					// new interval is contained within i... do nothing
					return;
				}
				// merge any subsequent overlapping intervals
				let j=i+1,
					delcount = 0;
				while (j<len && this.runs[j].start <= end) {
					// eventually extend end -- "union"
					end = Math.max(end, this.runs[j].end);
					delcount += 1;
					j += 1;
				}
				this.runs.splice(i+1, delcount);
				run.end = end;
				return;
			}
		}
		// append new interval
		this.runs.push({start: start, end: end});
	}
	insert (start, chars) {
		let i=0,
			len = this.runs.length,
			run;
		for (; i<len; i++) {
			run = this.runs[i];
			if (start < run.start) {
				// insertion point is fully before
				// insert new interval
				this.runs.splice(i, 0, {start: start, end: start+chars});
				len += 1;
				// shift subsequent intervals
				for (let j=i+1; j<len; j++) {
					this.runs[j].start += chars;
					this.runs[j].end += chars;
				}
				return;
			} else if (start <= run.end) {
				// insertion point intersects this interval, nb start may === run.end
				// extend this interval
				run.end += chars;
				// and shift all subsequent...
				for (let j=i+1; j<len; j++) {
					this.runs[j].start += chars;
					this.runs[j].end += chars;
				}
				return;
			}
		}
		// add new interval clean at the end
		this.runs.push({start: start, end: start+chars});				
	}
	insert_blank (start, chars) {
		// insert a sequence of characters *not* with the given attribute
		// (use when an inserting *another* attribute -- keeps attributes in parallel)
		let i=0,
			len = this.runs.length,
			run;
		for (; i<len; i++) {
			run = this.runs[i];
			if (start <= run.start) {
				// insertion point is before/at this interval
				// shift this & subsequent intervals
				for (let j=i; j<len; j++) {
					this.runs[j].start += chars;
					this.runs[j].end += chars;
				}
				return;
			} else if (start < run.end) {
				// start > run.start
				// insertion point inside this interval, nb start != run.end
				// split this interval at start
				this.runs.splice(i+1, 0, {start: start+chars, end: run.end+chars});
				len += 1;
				i+=1;
				// end current interval at the start of the split
				run.end = start;

				// this should happen above ?!
				// // and shift all subsequent...
				// for (let j=i+1; j<len; j++) {
				// 	this.runs[j].start += chars;
				// 	this.runs[j].end += chars;
				// }
				// return;
			}
		}
		// add new interval clean at the end
		// this.runs.push({start: start, end: start+chars});				
	}
	delete (start, chars) {
		let end = start + chars,
			i=0,
			len = this.runs.length,
			run;

		for (; i<len; i++) {
			// console.log(`RUN${i}`)
			run = this.runs[i];
			if (end <= run.start) {
				// console.log("CLEANBEFORE");
				// console.log("shifting all subsequent intervals")
				// cleanly before
				// shift all subsequent intervals -chars
				for (let j=i; j<len; j++) {
					this.runs[j].start -= chars;
					this.runs[j].end -= chars;
				}
				return;
			}

			// end >= run.start
			if (start < run.end) {
				// console.log("INTERSECTION");
				// new interval intersects i
				// nb: start might actually be before run.start
				if (start <= run.start) {
					if (end >= run.end) {
						// deletion area completely overlaps interval
						this.runs.splice(i, 1);
						len -= 1;
						i -= 1;
					} else {
						// deletion area overlaps start of interval
						run.start = start;
						run.end -= chars;
					}
				} else if (end <= run.end) {
					// deletion is within interval
					run.end -= chars;
				} else {
					// delection area overlaps end of interval
					run.end = start;
				}
			} else {
				// CLEAN AFTER
				// nothing more to do
				break;
			}
		}
	}
	unparse () {
		var ret = '';
		for (let i=0, len=this.runs.length; i<len; i++) {
			ret += `(${this.runs[i].start}, ${this.runs[i].end})\n`;
		}
		return ret;
	}
}



/*
var sr = new StyleRun();

sr.insert(0, 100);
sr.insert(200, 10);
console.log("---");
console.log(sr.unparse());
console.log("---");
sr.insert_blank(100, 5);
console.log(sr.unparse());
*/

// var pos = 1,
// 	del = 18;
// sr.delete(pos, del);
// console.log("---");
// console.log(`delete(${pos},${del})`)
// console.log("---");
// console.log(sr.unparse());

// should join
//sr.insert(10, 10);
//sr.insert(20, 10);



