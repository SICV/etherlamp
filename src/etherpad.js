import { StyleRun } from './stylerun.js';

async function wait (time) {
	return new Promise(resolve => {
		setTimeout(resolve, time);
	})
}

// e.g. Z:9kj>1|8=al=o4*1a|1+1$
function changeset_parse (c) {
	let changeset_pat = /^Z:([0-9a-z]+)([><])([0-9a-z]+)(.+?)\$/,
		op_pat = /(\|([0-9a-z]+)([\+\-\=])([0-9a-z]+))|([\*\+\-\=])([0-9a-z]+)/g,
		parse_op = m => {
			if (m[1]) {
				return {
					raw: m[0],
					op: ((m[3] == "+") ? "insert" : ((m[3] == "-") ? "delete" : "hold")),
					lines: parseInt(m[2], 36),
					chars: parseInt(m[4], 36)
				}
			} else if (m[5] == "*") {
				return {
					raw: m[0],
					op: "attr",
					index: parseInt(m[6], 36)
				}
			} else {
				return {
					raw: m[0],
					op: ((m[5] == "+") ? "insert" : ((m[5] == "-") ? "delete" : "hold")),
					chars: parseInt(m[6], 36)
				}				
			}
		};
	var m = changeset_pat.exec(c),
		bank = c.substring(m[0].length),
		ops_raw = m[4],
		op = null,
		ret = {
			raw: c,
			source_length: parseInt(m[1], 36),
			final_op: m[2],
			final_diff: parseInt(m[3], 36),
			ops_raw: ops_raw,
			ops: [],
			bank: bank,
			bank_length: bank.length
		};
		while (op = op_pat.exec(ops_raw)) {
			ret.ops.push(parse_op(op));
		}
		return ret;
}

class Text {
	constructor (text, attr) {
		this.text = text || '\n';
		this.attributes = attr || [];
	}
	perform_changeset (c) {
		// process the given change set -- modifying this object in place
		let textpos = 0,
			textline = 0,
			bank = c.bank,
			bankpos = 0,
			newtext = '',
			// newattributes = this.attributes.slice(),
			current_attributes = [];

		// loop through the operations
		// rebuilding the final text
		for (let i=0, ops=c.ops.length; i<ops; i++) {
			let op = c.ops[i];
			if (op.op == "attr") {
				// console.log(op);
				// record this attribute at current
				// add op to current attibutes to be applied to subsequent inserts
				// console.log("attr", op.index);
				current_attributes.push(op.index);
				// attributes.set(textpos, op.index)
			}
			if (op.op == "insert") {
				let newtextposition = newtext.length,
					insertion_text = bank.substring(bankpos, bankpos+op.chars);
				newtext += insertion_text;
				// advance positions
				bankpos += op.chars;

				current_attributes.forEach(a => {
					// console.log("ATTR", a);
					if (this.attributes[a] === undefined) {
						this.attributes[a] = new StyleRun();
					}
					// this.attributes[a].insert(newtextposition, op.chars);
				});

				for (var a in this.attributes) {
					console.log("INSERTATTR", a);
					if (this.attributes.hasOwnProperty(a)) {
						let sr = this.attributes[a];
						console.log("sr", sr);
						if (current_attributes.indexOf(parseInt(a)) > -1) {
							console.log("INSERT");
							sr.insert(newtextposition, op.chars);
						} else {
							console.log("BLANK")
							sr.insert_blank(newtextposition, op.chars);
						}
					}
				}

				// importantly, on an insert, the (original/old/previous) textpos
				// does *not* increment...
			} else if (op.op == "delete") {
				let newtextposition = newtext.length; // is this right?
				// todo: delete attributes as well
				current_attributes.forEach(a => {
					if (this.attributes[a] === undefined) {
						return;
					}
					this.attributes[a].delete(newtextposition, op.chars);
				});
				textpos += op.chars;
			} else if (op.op == "hold") {
				newtext += this.text.substring(textpos, textpos+op.chars);
				textpos += op.chars;
			}
		}
		// append rest of old text...
		newtext += this.text.substring(textpos);
		this.text = newtext;
		// return new Text(newtext, newattributes);
	}
	get_text () {
		return this.text;
	}
	get_html () {
		return StyleRun.render_runs(this.attributes, this.text);
	}
}

export class Etherpad {

	static async load (filename, padname) {
		// console.log("loading json...");
		let resp = await fetch(filename),
			data = await resp.json(),
			pad_data = data[`pad:${padname}`];
		var pad = new Etherpad(padname);
		pad.json = data;
		pad.parse_json(data);
		return pad;
	}

	constructor (padname) {
		this.padname = padname;
		this.text = "\n";
		this.changesets = null;
		//this.revs = new Array();
		this.curtext = new Text("\n");
		this.rev = -1;
	}

	next () {
		if (this.rev < this.last_rev) {
			this.rev += 1;
			this.curtext.perform_changeset(this.changesets[this.rev]);
		}
	}

	// calculate_revisions () {
	// 	var text = new Text("\n");
	// 	for (var i=0; i<=this.last_rev; i++) {
	// 		text = this.revs[i] = text.perform_changeset(this.changesets[i]);
	// 	}
	// 	console.log("calculated revisions", this.revs);
	// }

	// get_revision (n) {
	// 	return this.revs[n];
	// }

	async animate (callback, steptime) {
		var curtext = "\n";
		for (var i=0; i<=this.last_rev; i++)  {
			let change = this.perform_changeset_curline(curtext, this.changesets[i]);
			change.rev = i;
			callback(change);
			curtext = change.text;
			// console.log(curtext);
			await wait(steptime||100);
		}
		//console.log("CHECK", (curtext == pad_data.atext.text))
	}

	async animate2 (callback, steptime) {
		var text = new Text("\n");
		for (var i=0; i<=this.last_rev; i++)  {
			let newtext = text.perform_changeset(this.changesets[i]);
			// change.rev = i;
			callback({
				'html': newtext.get_html(),
				'rev': i
			});
			text = newtext;
			// console.log(curtext);
			await wait(steptime||100);
		}
		return text;
		//console.log("CHECK", (curtext == pad_data.atext.text))
	}

	parse_json (data) {
		let pad_data = data[`pad:${this.padname}`],
			last_rev = pad_data.head;

		// parse the changesets / revisions
		this.changesets = [];
		this.last_rev = last_rev;
		// console.log("loaded");
		// last_rev = 10;
		for (let r = 0; r<=last_rev; r++) {
			let d = data[`pad:${this.padname}:revs:${r}`],
				cs = changeset_parse(d.changeset);
			// meta.author + meta.timestamp
			cs.meta = d.meta;
			this.changesets.push(cs)
		}
		// console.log("SAME", (this.text == pad_data.atext.text));
		// console.log("done", last_rev)		
		return last_rev;
	}

	get_attrib (n) {
		return this.json.pool.numToAttrib[""+n];
	}

	perform_changeset (input_text, c) {
		let textpos = 0,
			textline = 0,
			bank = c.bank,
			bankpos = 0,
			newtext = '';

		// loop through the operations
		// rebuilding the final text
		for (let i=0, ops=c.ops.length; i<ops; i++) {
			let op = c.ops[i];
			if (op.op != "attr") {
				// console.log(op);
			}
			if (op.op == "insert") {
				newtext += bank.substring(bankpos, bankpos+op.chars);
				// advance positions
				bankpos += op.chars;
				// importantly, on an insert, the (original/old/previous) textpos
				// does *not* increment...
			} else if (op.op == "delete") {
				textpos += op.chars;
			} else if (op.op == "hold") {
				newtext += input_text.substring(textpos, textpos+op.chars);
				textpos += op.chars;
			}
		}
		// append rest of old text...
		newtext += input_text.substring(textpos);
		return newtext;
	}

	perform_changeset_curline (input_text, c) {
		let textpos = 0,
			textline = 0,
			bank = c.bank,
			bankpos = 0,
			newtext = '',
			curline = '';

		// loop through the operations
		// rebuilding the final text
		for (let i=0, ops=c.ops.length; i<ops; i++) {
			let op = c.ops[i];
			if (op.op != "attr") {
				// console.log(op);
			}
			if (op.op == "insert") {
				newtext += bank.substring(bankpos, bankpos+op.chars);
				// advance positions
				//if (!op.lines) {
					curline += "<span class=\"insert\">"+bank.substring(bankpos, bankpos+op.chars)+"</span>";
				//}
				bankpos += op.chars;
				// importantly, on an insert, the (original/old/previous) textpos
				// does *not* increment...
			} else if (op.op == "delete") {
				textpos += op.chars;
			} else if (op.op == "hold") {
				newtext += input_text.substring(textpos, textpos+op.chars);
				if (!op.lines) {
					curline += "<span class=\"hold\">"+input_text.substring(textpos, textpos+op.chars)+"</span>";
				}
				textpos += op.chars;
			}
		}
		// append rest of old text...
		newtext += input_text.substring(textpos);
		var rest_of_line = input_text.substring(textpos);
		if (rest_of_line.indexOf("\n")) {
			rest_of_line = rest_of_line.substring(0, rest_of_line.indexOf("\n"));
		}
		curline += "<span class=\"rest\">"+rest_of_line+"</span>";
		return {text: newtext, curline: curline};
	}
}

