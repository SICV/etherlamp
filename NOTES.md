Revision 0 is the setting of the initial contents of pad are set with a single "multi-line" operation:
The source_length is 1 (the implicit newline?).
{
	source_length: 1,
	final_op: ">",
	final_diff: 414
	ops_raw: "|7+bi",
	bank: "Welcome to Etherpad \n\nThis pad is synchronized as you type ... settings.json\n",
	bank_length: 414,
	ops: [{
		lines: 7,
		chars: 414,
		op: "lines",
		code: "+"
	}
	]
}

As a line operation, the bank ends with a newline.
Could call this insert_lines?!


Next, I selected all text and blanked it, this is represented by:

{
	source_length: 415,
	final_op: "<",
	final_diff: 414,
	ops_raw: "|7-bi",
	bank: ""
	op: [{
		op: "lines",
		code: "-",
		lines: 7,
		chars: 414
	}]
}

delete_lines

? simple rep op: (insert, delete, hold) with: [lines], chars

Each change set completely describes the changes to the text starting from the top. It however ends at the last change. The =N operation "holds" content that is static.
As the line based operations must end in a newline, the initial hold is often a combination of (hold N lines / x chars) folowed by (hold N chars).


Fonts
==========

Using:

* otf2bdf to convert otf to bdf at a particular point size and then
* pilfont.py to convert bdf to pil

Matrix
===========

Experimenting with chaining, this works:

	sudo python runtext.py --led-cols 64 --led-rows 32 --led-slowdown-gpio=2 -c 2

