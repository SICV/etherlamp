// rollup.config.js
// https://github.com/rollup/rollup-plugin-commonjs
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';

export default [{
  input: 'src/etherpad.js',
  output: {
    file: 'dist/etherpad.js',
    format: 'iife',
    name: 'etherpad'
  },
  plugins: [
    resolve(), 
    commonjs()
  ]
}];
